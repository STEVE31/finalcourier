<!DOCTYPE html>
<html> 
<head>
<title>Employee Details Upload</title>
<link rel="stylesheet" type="text/css" href="style1.css">
<style type="text/css">
#content{
  width:50%;
  margin:20px auto;
  border : 1px solid #cbcbcb;
}
form {
  width : 50%;
  margin : 20px auto;
}
form div{
  margin-top : 5px;
}
#img_div{
  width: 80%;
  padding : 5px;
  margin : 15px auto;
  border : 1px solid #cbcbcb;
}
#img_div:after{
  content : "";
  display: block;
  clear : both;
}
img{
  float: left;
  margin: 5px;
  width: 300px;
  height: 140px;
}
</style>
</head>
<body>
<div id="content">


  <form method="POST" action="add_dep.php" enctype="multipart/form-data">
  	<input type="hidden" name="size" value="1000000">
  	<div>
          <h2>Employee Details </h2>
      <input type="text" name="name" placeholder="Full Name" required="required"><br><br>
      <input type="text" name="username" placeholder="Username" required="required"><br><br>
      <input type="password" name="password" placeholder="Password" required="required"><br><br>
      <input type="text" name="email" placeholder="Email" required="required"><br><br>
      <input type="number" name="idno" placeholder=" ID Number" required="required"><br><br>
      <input type="number" name="contact" placeholder="Phone number" required="required"><br><br>
  	<div>
      <button type="submit" name="submit">add</button>
      <a href="allemployees.php">View all</a>
  	</div>
  </form>
  
</div>
</body>
</html>