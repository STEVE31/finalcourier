<!DOCTYPE html>
<html>
    <head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>Employees</title>
    </head>
    <div class="container">
        <h4>Here are the registered employees</h4>
        <div class="row">
        <?php
            $conn = mysqli_connect("localhost:3306","root","","courier-services");
            $sql =  "SELECT * from `employees`";
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
            ?>
                    <div class="col-sm-6 col-md-4 col-lg-3 col-xl-3 colum">
                        <p>Full Name: <?php echo $row["fullname"]?></p>
                        <p>Username: <?php echo $row["username"]?></p>
                        <p>Email:</br> <?php echo $row["email"]?></p>
                        <p>Phone No: <?php echo $row["phoneno"]?></p>
                        <p>ID No: <?php echo $row["idno"]?></p>
                    </div>
            <?php
                }
            }else {
                echo "<h3> No vehicles have been registered</h3>";
            }
            ?>
                </div>
                <a href="add_employee.php">Add employee</a>
            </div>
            
        </div>
    </div>
    <style>
        .colum{
            background-color: blueviolet;
            border: 2px solid white;
            border-radius: 10px;
            color: white;
        }
    </style>
</html>