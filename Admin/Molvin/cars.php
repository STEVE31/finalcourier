<!DOCTYPE html>
<html>
    <head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>Cars</title>
    </head>
    <div class="container">
        <h4>Here are the available transportation vehicles</h4>
        <div class="row">
    <?php
    $conn = mysqli_connect("localhost:3306","root","","courier-services");
    $sql =  "SELECT * from `vehicle`";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
    ?>
            <div class="col-sm-6 col-md-4 col-lg-3 col-xl-3 colum">
                <p>Brand: <?php echo $row["brand"]?></p>
                <p>Plateno: <?php echo $row["platenumber"]?></p>
                <p>Driver: <?php echo $row["drivername"]?></p>
            </div>
    <?php
        }
    }else {
        echo "<h3> No vehicles have been registered</h3>";
    }
    ?>
        </div>
        <a href="vehicle.php">Add vehicle</a>
    </div>
    <style>
        .colum{
            background-color: blueviolet;
            border: 2px solid white;
            border-radius: 10px;
            color: white;
        }
    </style>
</html>