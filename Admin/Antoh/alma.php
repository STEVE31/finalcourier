<!DOCTYPE html>

<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <nav class="navbar navbar-inverse fixed-top">
        <div class="container-fixed">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.jsp">Courier-Services</a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li><a href="farmshop.jsp">Shop</a></li>
                    <li><a href="postprod.jsp">Post product</a></li>
                    <li><a href="myproducts.jsp">My products</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="farmregister.jsp"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                    <li><a href="farmlogin.jsp"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</body>

</html>