-<!--
-** Author - Antony Nyaga 
-** Project - Courier Services
-** Section - Admin
-** Description - This page is navigated to other sections through iframes
--->
<section class="mt-2 pr-3 pl-3 mb-0">

    <!-- JS IMPORTS -->
    <!--importation of bootsrap classes -->
    
    
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <!-- End of bootsrap classes imports -->
    <!-- END OF JS IMPORTS -->

    <!-- JS IMPORTS -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- END OF JS IMPORTS -->
        <div class="container-fluid">

            <div class="row">
                <div class="col-sm-12">
                    <div id="my-slider" class="carousel slide" data-ride="carousel">

                        <!-- indicatore dot nov-->
                        <ol class="carousel-indicators">
                            <li data-target="#my-slider" data-slide-to="0" class="active"></li>
                            <li data-target="#my-slider" data-slide-to="1"></li>
                            <li data-target="#my-slider" data-slide-to="2"></li>
                          
                        </ol>

                        <!-- wrapper for slides-->
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <img src="images/1.jpeg" alt="fruits" style="width:100%" />
                                <div class="carousel-caption">
                                    <h1>Our services are reliable</h1>
                                </div>
                            </div>
                            <div class="item ">
                                <img src="images/2.jpg" alt="fruits" style="width:100%" />
                                <div class="carousel-caption">
                                    <h1>Fast and convenient transport service</h1>
                                </div>
                            </div>
                            <div class="item ">
                                <img src="images/3.jpg" alt="fruits" style="width:100%" />
                                <div class="carousel-caption">
                                    <h1>Freight service available on demand</h1>
                                </div>
                            </div>

                        </div>
                        <!-- control or next and prev buttons-->
                        <a class="left carousel-control" href="#my-slider" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>

                        <a class="right carousel-control" href="#my-slider" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                         <!-- End of control or next and prev buttons-->

                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
<div class="Themes">
        <div class="container">
            <div class="feature-grids">

                <div class="col-md-3 feature-grid">
                    <h3><span class="fea-icon"> </span> WHO WE ARE</h3>
                    <p> Courier services (TUK COURIER/COURIER KENYA) is a fast growing Courier services rendering company with interests in  customs clearing,  trucking and distribution across Kenya and East Africa at large..</p>
                    <a href="about.php" class="arrow_btn">Read More</a>
                </div>

               
            </div>
        </div>
    <!---//End-da-features----->
