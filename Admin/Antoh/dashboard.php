-<!--
-** Author - Antony Nyaga 
-** Project - Courier Services
-** Section - Admin
-** Description - This is the Dashboard page
--->
<!DOCTYPE html>
<html>

<head>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="style.css">
	<script src="new.js"></script>
</head>

<body>
	<nav class="navbar navbar-inverse fixed-top">
		<div class="container-fixed">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Courier-services</a>
			</div>
			<div class="collapse navbar-collapse" id="myNavbar">
				
				<!-- <ul class="nav navbar-nav navbar-right">
					<li><a href="#"><span class="glyphicon glyphicon-user"></span>Admin</a></li>
					<li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
					</li>
				</ul> -->
			</div>
		</div>
	</nav>
	<div class="container-fluid main-container">
		<div class="col-md-2 sidebar">
			<div class="row">
				<!-- uncomment code for absolute positioning tweek see top comment in css -->
				<div class="absolute-wrapper">

				</div>
				<!-- Menu -->
				<div class="side-menu">
					<nav class="navbar navbar-default" role="navigation">
						<!-- Main Menu -->
						<div class="side-menu-container">
							<ul class="nav rada radas navbar-nav">
								<li id="text" class="Dashboard active rada"><a href=# id="Dashboard" onclick="setactive()"><span class="glyphicon glyphicon-dashboard"></span> Dashboard</a></li><br>
								<!--<li class="Locale rada"><a href="#" id="Locations" onclick="setactive()"><span class="glyphicon glyphicon-send"></span> Admin</a></li>-->
								<li class="Work rada"><a href="#" id="Workflow" onclick="setactive()"><span class="glyphicon glyphicon-star"></span> Work flow</a></li><br>
								<li class="Car rada"><a href="#" id="Vehicle" onclick="setactive()"><span class="glyphicon glyphicon-star"></span> Vehicle management</a></li><br>
								<li class="Use rada"><a href="#" id="User" onclick="setactive()"><span class="glyphicon glyphicon-user"></span> User management</a></li><br>
								<li class="Office rada"><a href="#" id="Offices" onclick="setactive()"><span class="glyphicon glyphicon-hdd"></span> Offices</a></li><br>
								<!--<li class="Locale rada"><a href="#" id="Locations" onclick="setactive()"><span class="glyphicon glyphicon-send"></span> Locations</a></li>-->
								<li class="Payment rada"><a href="#" id="Payment" onclick="setactive()"><span class="glyphicon glyphicon-phone"></span> Payment</a></li>
								<li>
								<footer class="pull-left footer">
									<p class="col-md-12">
										<hr class="divider">
										Copyright &COPY; 2019 <a href="">Courier-services</a>
									</p>
								</footer>
								</li>
							</ul>
						</div><!-- /.navbar-collapse -->
					</nav>

				</div>
			</div>
		</div>
		<div class="col-md-10 content">
			<div class="container-fluid">
				<div class="panel-heading">

				</div>
				<div class="panel-body">
					<iframe id="frames" height="900px" width="1200px" src="landing.php"></iframe>
				</div>
			</div>
		</div>
		
	</div>



</body>

</html>