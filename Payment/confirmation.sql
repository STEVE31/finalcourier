-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 28, 2019 at 03:55 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `meadow`
--

-- --------------------------------------------------------

--
-- Table structure for table `confirmation`
--

CREATE TABLE `confirmation` (
  `checkoutID` varchar(70) NOT NULL,
  `resultsdescription` varchar(70) NOT NULL,
  `responsedescription` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `confirmation`
--

INSERT INTO `confirmation` (`checkoutID`, `resultsdescription`, `responsedescription`) VALUES
('ws_CO_DMZ_420509321_29032019172902131', '[STK_CB - ]Request cancelled by user', ''),
('ws_CO_DMZ_420523846_29032019174426823', '[STK_CB - ]Request cancelled by user', ''),
('ws_CO_DMZ_420527460_29032019174812478', '[STK_CB - ]Request cancelled by user', ''),
('ws_CO_DMZ_420528479_29032019174919990', '[STK_CB - ]Request cancelled by user', ''),
('ws_CO_DMZ_420532489_29032019175406723', '[STK_CB - ]Request cancelled by user', ''),
('ws_CO_DMZ_420536122_29032019175726651', '[STK_CB - ]Request cancelled by user', ''),
('ws_CO_DMZ_420539128_29032019180030073', '[STK_CB - ]Unable to lock subscriber, a transaction is already in proc', ''),
('ws_CO_DMZ_283921824_29032019175849900', '[STK_CB - ]SMSC ACK timeout.', ''),
('ws_CO_DMZ_420540832_29032019180229587', '[STK_CB - ]Request cancelled by user', ''),
('ws_CO_DMZ_283933248_29032019180739479', '[STK_CB - ]Request cancelled by user', ''),
('ws_CO_DMZ_283937976_29032019181247837', '[STK_CB - ]Request cancelled by user', ''),
('ws_CO_DMZ_420560341_29032019182052210', '[STK_CB - ]Request cancelled by user', ''),
('ws_CO_DMZ_420591771_29032019184711883', '[STK_CB - ]Request cancelled by user', ''),
('ws_CO_DMZ_283978414_29032019184959846', 'The service request is processed successfully.', ''),
('ws_CO_DMZ_420600457_29032019185532926', 'The service request is processed successfully.', ''),
('ws_CO_DMZ_420604694_29032019190005552', '[STK_CB - ]Request cancelled by user', ''),
('ws_CO_DMZ_420607501_29032019190252709', '[STK_CB - ]Request cancelled by user', ''),
('ws_CO_DMZ_283992526_29032019190340374', 'The service request is processed successfully.', ''),
('ws_CO_DMZ_445128872_16042019162313633', '[STK_CB - ]Request cancelled by user', ''),
('ws_CO_DMZ_445140601_16042019163520862', '[STK_CB - ]Request cancelled by user', ''),
('ws_CO_DMZ_445146848_16042019164126467', '[STK_CB - ]Request cancelled by user', ''),
('ws_CO_DMZ_307285037_16042019164252035', '[STK_CB - ]Request cancelled by user', ''),
('ws_CO_DMZ_307286681_16042019164436337', '[STK_CB - ]Request cancelled by user', ''),
('ws_CO_DMZ_307287207_16042019164507358', '[STK_CB - ]Request cancelled by user', ''),
('ws_CO_DMZ_445152717_16042019164720674', '[STK_CB - ]Request cancelled by user', ''),
('ws_CO_DMZ_307290598_16042019164843809', '[STK_CB - ]Request cancelled by user', ''),
('ws_CO_DMZ_445157769_16042019165216547', '[STK_CB - ]Request cancelled by user', ''),
('ws_CO_DMZ_445158518_16042019165308050', 'The service request is processed successfully.', ''),
('ws_CO_DMZ_307301120_16042019165756819', '[STK_CB - ]Request cancelled by user', ''),
('ws_CO_DMZ_445170660_16042019170641450', '[STK_CB - ]Request cancelled by user', ''),
('ws_CO_DMZ_445173192_16042019170951563', '[STK_CB - ]Request cancelled by user', ''),
('ws_CO_DMZ_307317003_16042019171328072', '[STK_CB - ]Request cancelled by user', ''),
('ws_CO_DMZ_445176920_16042019171416621', '[STK_CB - ]Request cancelled by user', ''),
('ws_CO_DMZ_445177223_16042019171435548', '[STK_CB - ]Request cancelled by user', ''),
('ws_CO_DMZ_446149693_17042019141920873', '[STK_CB - ]Request cancelled by user', ''),
('ws_CO_DMZ_446150475_17042019142008551', 'The service request is processed successfully.', '');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
