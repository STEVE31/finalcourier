<?php

namespace payment\Mpesa\C2B;

use GuzzleHttp\Exception\RequestException;
use InvalidArgumentException;
use payment\Mpesa\C2B\Mpesa\Engine\Core;
use payment\Mpesa\Exceptions\ErrorException;
use payment\Mpesa\C2B\Mpesa\Repositories\ConfigurationRepository;
use payment\Mpesa\C2B\Mpesa\Traits\MakesRequest;

/**
 * Class Simulate.
 *
 * @category PHP
 *
 * @author   mukhebi <payment\Mpesa\C2Bprivate@gmail.com>
 *
 * @method Simulate from(string $number)
 * @method Simulate request(string $amount)
 * @method stdClass push(string $amount = null, string $number = null, string $reference = null, string $command = null, string $account = null)
 * @method Simulate setCommand(string $command)
 * @method Simulate usingAccount(string $account)
 * @method Simulate usingReference(string $reference)
 * @method stdClass validate(string $checkoutRequestID, string $account = null)
 */
class Simulate
{
    use MakesRequest;

    /**
     * The simulation number
     *
     * @var string
     */
    protected $number;

    /**
     * The transaction amount
     *
     * @var string
     */
    protected $amount;

    /**
     * The transaction reference
     *
     * @var string
     */
    protected $reference;

    /**
     * The account to be used
     *
     * @var string
     */
    protected $account = null;

    /**
     * The transaction command to be used.
     *
     * @var string
     */
    protected $command = CUSTOMER_PAYBILL_ONLINE;

    /**
     * Set the request amount to be deducted.
     *
     * @param int $amount
     *
     * @return $this
     */
    public function request($amount)
    {
        if (!\is_numeric($amount)) {
            throw new \InvalidArgumentException('The amount must be numeric');
        }

        $this->amount = $amount;

        return $this;
    }

    /**
     * Set the Mobile Subscriber Number to deduct the amount from.
     * Must be in format 2547XXXXXXXX.
     *
     * @param int $number
     *
     * @return $this
     */
    public function from($number)
    {
        if (! starts_with($number, '2547')) {
            throw new \InvalidArgumentException('The subscriber number must start with 2547');
        }

        $this->number = $number;

        return $this;
    }

    /**
     * Set the product reference number to bill the account.
     *
     * @param int $reference
     *
     * @return $this
     */
    public function usingReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Set the account to be used.
     *
     * @param string $account
     *
     * @return self
     */
    public function usingAccount($account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Set the unique command for this transaction type.
     *
     * @param string $command
     *
     * @return self
     */
    public function setCommand($command)
    {
        if (! in_array($command, VALID_COMMANDS)) {
            throw new InvalidArgumentException('Invalid command sent');
        }

        $this->command = $command;

        return $this;
    }

    /**
     * Prepare the transaction simulation request
     *
     * @param int    $amount
     * @param int    $number
     * @param string $reference
     * @param string $command
     *
     * @throws ErrorException
     *
     * @return mixed
     */
      try {
            $response = $this->makeRequest(
                $body,
                Core::instance()->getEndpoint(MPESA_REGISTER, $account),
                $account
            );

            return \json_decode($response->getBody());
        } catch (RequestException $exception) {
            $message = $exception->getResponse() ?
               $exception->getResponse()->getReasonPhrase() :
               $exception->getMessage();

            throw new Exception($message);
        }
}
