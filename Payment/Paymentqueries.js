var checkoutID = $("#checkoutID").text() ;
console.log(checkoutID);


//import Spinner from '/mpesa/spin.js';

var opts = {
	lines: 10, // The number of lines to draw
	length: 56, // The length of each line
	width: 16, // The line thickness
	radius: 57, // The radius of the inner circle
	scale: 0.65, // Scales overall size of the spinner
	corners: 1, // Corner roundness (0..1)
	color: '#80ff00', // CSS color or array of colors
	fadeColor: 'transparent', // CSS color or array of colors
	speed: 1, // Rounds per second
	rotate: 44, // The rotation offset
	animation: 'spinner-line-fade-more', // The CSS animation name for the lines
	direction: 1, // 1: clockwise, -1: counterclockwise
	zIndex: 2e9, // The z-index (defaults to 2000000000)
	className: 'spinner', // The CSS class to assign to the spinner
	top: '20%', // Top position relative to parent
	left: '50%', // Left position relative to parent
	shadow: '0 0 1px transparent', // Box-shadow for the lines
	position: 'absolute' // Element positioning
  }
  var target = document.getElementById('spinner');
  var spinner = new Spinner(opts).spin(target);


function ajaxCall () {
	$.ajax({
	timeout: 25000 ,
	url:'/mpesa/paymentcheck.php',
	type:'POST',
	data:{"checkoutID":checkoutID},
	datatype:'json',
	success: function(response)
	{
		console.log("Success");
		console.log(response);
		
		//var obj = JSON.parse(response);
		spinner.stop();
		if(response['resultsdescription'].includes('success'))
		{
			$('#payconfirm').text('Payment Confirmed');
		}
		else
		{
			$('#payconfirm').text(response['resultsdescription']);
		}
	},
	error: function(err)
	{
		spinner.stop();
		console.log('error');
		console.log(err);
		$('#payconfirm').text("Error Fetching Payment Details");
	}
});
}

setTimeout( ajaxCall,25000);

// To show time passing in the Console
var i = 1;
setInterval(function(){
	console.log(1);
	i++;
}, 1000);
