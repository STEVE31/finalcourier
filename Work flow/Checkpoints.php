<!--
** Author - Eric Kilonzo 
** Project - Courier Services
** Section - Workflow on Checkpoints
** Description - This page is navigated only when the product has been dispatched
-->
 <!--start of program code-->

 <!--php file starts here for the database-->
<?php
ob_start();
session_start();
$con = mysqli_connect("localhost:3306","root","","courier-services");

// Checking database connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }
  else{
      echo "send";
  }

  $itemid = $_SESSION['checkpointid'];
  echo  $itemid;

  $sql = " SELECT * from shippingdetails s 
  inner join parcel p on s.parcelid = p.id
  inner join offices o on s.officeid = o.id
  inner join offices of on s.destinationid = of.id
  inner join users u on s.senderid = u.id
  inner join users us on s.recepientid = us.id
  inner join vehicle v on s.vehicleid = v.id where sid = '$itemid'";
  $result = $con->query($sql);

  mysqli_query($con, $sql);
  if ($result){
  echo "no";
  }else{
  echo "yes";
  }
 ?>
<!--end of the php program code-->

<!--html type below-->
<!DOCTYPE html>
<!--start of html tag-->
<html>
<head>
<title>Speedy Courier Checkpoints</title>

<!--link to the local external database css below-->
<link rel="stylesheet" type="text/css" href="Checkpoints.css">

<!--importation of the online bootstrap classes start-->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<!--the extended bootstrap classes below-->
<script src="https://code.jquery.com/jquery-3.3.1.slim. min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!--end of online bootstrap classes-->

</head>
<!--start of the body tag-->
<body> 
  <!--division--> 
<div id="heading">
  <!--heading division-->
<h1>Courier-Services Checkpoints</h1>
<!--heading one start and end tags-->
</div>
<!--end of division for the head-->
<?php
      if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
          echo "";
      
        $_SESSION['currcheck'] = $row["checkpoint"];
    ?>

<!--divison contains the class container-->
<div class="container">
  <div class="row">
    <div class="col-sm-7">
      <div class="container">
        <?php
                    echo '<img src="data:image/jpeg;base64,'.base64_encode( $row['image'] ).'" height = "100%" width ="100%"/>';
                    ?>
      </div>
    </div>


    <div class="col-sm-5">
  <form action="Checkpoint.php">
  <div class="form-group">
    <label for="Item">Item Name:</label>
    <input type="text" class="form-control"placeholder="<?php echo $row["name"] ?>"  name="item_name">

  </div>
  <div class="form-group">

    <label for="Item">Sender Name:</label>
    <input type="text" class="form-control" placeholder="<?php echo $row["username"]?>" name="sender_name">
  </div>
  <div class="form-group">
    <label for="Item">Sender Location:</label>
    <input type="text" class="form-control" placeholder="<?php echo $row["location"]?>" name="sender_location">

  </div>
  <div class="form-group">
    <label for="Item">Sender Cell No.:</label>
    <input type="text" class="form-control" placeholder="<?php echo $row["telephone"]?>" name="sender_cell_no">
  </div>
  <div class="form-group">
    <label for="Item">Recepient Name:</label>
    <input type="text" class="form-control" placeholder="<?php echo $row["firstname"]?>" name="recepient_name">
  </div>
  <div class="form-group">
    <label for="Item">Recepient Location:</label>
    <input type="text" class="form-control" placeholder="<?php echo $row["location"]?>" name="sender_location">
  </div>
  <div class="form-group">
    <label for="Item">Recepient Cell No.:</label>
    <input type="text" class="form-control" placeholder="<?php echo $row["telephone"]?>" name="recepient_cell_no">
  </div>
   <button type="submit" name="button" class="btn btn-primary">Checkpoint Status Update</button>
</form>
<?php
            }
          }

        ?>
    </div>
  </div>
</div>
<!--start of the notification script-->
<script type="text/javascript">
  document.getElementsByTagName("button")[0].
  addEventListener("click",
   function (e) {
     alert("Checkpoint status updated"); 
});
</script>
<!--end of notification script-->
<!--end of body tag-->
</body>
<!--closing of html tag-->
</html>

