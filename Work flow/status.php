<!DOCTYPE html>
<html>

<!--
** Author - Steve Nginyo 
** Project - Courier Services
** Section - Workflow
** Description - Acquiring on transit objects for display from the database
                Product origin and destination are viewed
                Product sender and recipient are also displayed
                Viewing current status of the product is available here
                Products are manually dispatched when placed in cars
                At each checkpoint the product is marked that it has passed
                The last checkpoint is the destination
                The product is the marked arrived when it reaches the destination
-->

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Speedy Courier Status</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="status.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<!--
    * Importation of bootstrap online classes is done here
    * links to local css and javascript file is also done here
    * bootstrap helps in styling of the web page content
-->

</head>

<?php

//creation of connection to the database

$conn = mysqli_connect("localhost:3306","root","","courier-services");

/** 
* Check connection or link to the database is done or available display a message
* A message is displayed when the database connection is successful
* An error message is also displayed if the database connection is not successful
*/

if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }
  else{
      //echo "Worked";
  }

/**
 * Database tables are queried
 * the table contains foreign keys
 * the tables are therefore joined
 * this helps query several databases at the same time
 * the result is then saved in a variable result
 */

  $sql = " SELECT sid, p.name, u.username,
            p.id as parcelid,
            paid,
            amount,
            p.image as image,
            us.username as ususername, 
            v.platenumber, o.location, 
            of.location as oflocation, 
            checkpoint from shippingdetails as s
  join parcel as p on s.parcelid = p.id
  join offices as o on s.officeid = o.id
  join offices as of on s.destinationid = of.id
  join users as u on s.senderid = u.id
  join users as us on s.recepientid = us.id
  join vehicle as v on s.vehicleid = v.id ";

  $result = $conn->query($sql);

/**
 * This commented code confirms the execution of the query 
 * in case the syntax is correct
 * there may be no syntax error 
 * but the query may not have executed
 */

  /* CODE TO CHECK QUERY EXECUTION
    mysqli_query($conn, $sql);
    $result1 = $conn->query($sql1);
    if ($result){
    echo "no";
    }else{
    echo "yes";
    }
  */
?>

<body>
    <div class="jumbotron">
        <h1 class="display-5">View current status of the product sent</h1>
        <p class="led">Tracking of the current status by the work flow sections of the project ie. 
        customer care, dispatch, checkpoints and destination</p>
        <hr class="my-2">
        <p>Edit product status on dispatch, arrival and at checkpoints</p>
    </div>
    <div class="container-fluid">
        <?php
        /**
         * Checks whether there is a result set from the database
         * if there is a result set the table is created 
         * the table header is then created here
         * this is before the result set iteration
         * The details of the product including
         * it's id, name, sender, receiver, dispatch, 
         * current checkpoint, current status of the product,
         * vehicle it is on, origin and destination are all
         * displayed 
          */
            if ($result->num_rows > 0) {
        ?>
        <table class="table table-dark table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Item</th>
                    <th>Sender</th>
                    <th>Receiver</th>
                    <th>Payment status</th>
                    <th>Dispatch</th>
                    <th>Checkpoint</th>
                    <th>Arrival</th>
                    <th>Vehicle</th>
                    <th>Origin</th>
                    <th>Destination</th>
                    <th>Amount</th>
                </tr>
            </thead>
            <tbody>
            <?php
            /**
             * a new iteration without the table header is created
             * the iteration is supposed to display items of the result set
             * the iteration creates a new table row
             * then it inputs the values from the designated table headers
             * into the table columns within the row
             * the iteration happens  for each result in the set creating
             * multiple rows
             */
            // output data of each row
            while($row = $result->fetch_assoc()) {
            ?>          
                <tr>
                    <td><?php echo  $row["sid"]?></td>
                    <td><?php echo  $row["name"]?></td>
                    <td><?php echo  $row["username"]?></td>
                    <td><?php echo  $row["ususername"]?></td>
                    <td><?php echo  $row["paid"]?></td>
                    
                    <td>
                    <?php
                    //checking for the current checkpoint of the product from table
                    if($row["checkpoint"] == 0 ||  $row["checkpoint"] < 0){
                        /**
                         * If the checkpoint is zero it means the product 
                         * has not been dispatched yet
                         */
                        echo "Not dispatched";
                    }else{
                        /**
                         * Otherwise the product has been dispatched
                         * and is enroute to the first checkpoint
                         */
                        echo "Dispatched";
                    }
                    ?>
                    </td>

                    <td><?php echo  $row["checkpoint"]?> of 6</td>
                    
                    <td>
                    <?php
                    //checks for current checkpoint of the product from table
                    if($row["checkpoint"] == 6 ||  $row["checkpoint"] > 6){
                        /**
                         * If the product has reached the sixth checkpoint it means it has arrived
                         * to the final checkpoint
                         * therefore it is marked arrived
                         */
                        echo "Arrived";
                    }else{
                        /**
                         * If the product has yet to arrive at the final checkpoint 
                         * the message of the product not having arrived is displayed
                         */
                        echo "Not yet arrived";
                    }
                    ?>
                    </td>

                    <td><?php echo  $row["platenumber"]?></td>
                    <td><?php echo  $row["location"]?></td>
                    <td><?php echo  $row["oflocation"]?></td>
                    <td><?php echo  $row["amount"]?></td>
                    <td>
                        <form action="todispatch.php">

                            <!--
                                a hidden input for a form is created with
                                a value of the id of the product from the database
                             -->

                            <input type="hidden" name="dispatch" value="<?php echo  $row["sid"]?>">
                            <?php

                            //checks whether the product has been dispatched

                            if($row["checkpoint"] == 0 ||  $row["checkpoint"] < 0){

                                /**
                                 * if it has yet to be dispatched the button is activated
                                 * the form with the hidden input 
                                 * is redirected to "todispatch.php"
                                 */

                            ?>
                                <button class="btn btn-primary btn-sm" type="submit" method="post">Dispatch</button>
                            <?php
                            }else{

                                /**
                                 * the button is deactivated/disabled if the product has already been dispatched
                                 */

                            ?>
                               <button class="btn btn-primary btn-sm" type="submit" disabled>Dispatch</button>
                            <?php
                            }
                            ?>
                        </form>
                    </td>

                    <td>
                        <form action="tocheckpoint.php">
                            <input type="hidden" name="checkpoint" value="<?php echo  $row["sid"]?>">
                            <?php
                            //checks the current status of the product
                            if($row["checkpoint"] > 0 &&  $row["checkpoint"] < 5){
                                /**
                                 * if the product has not arrived and is dispatched
                                 * the checkpoint button is enabled
                                 * the form redirects to "to checkpoint.php"
                                 */
                            ?>
                                <button class="btn btn-primary btn-sm" type="submit" method="post">Checkpoint</button>
                            <?php
                            }else{
                                /**
                                 * the product is not dispatched 
                                 * therefore cannot go through the checkpoints 
                                 * or the product has arrived
                                 * therefore has gone though all the checkpoints
                                 */
                            ?>
                               <button class="btn btn-primary btn-sm" type="submit" disabled>Checkpoint</button>
                            <?php
                            }
                            ?>
                        </form>
                    </td>

                    <td>
                        <form action="topay.php">

                            <!--
                                a hidden input for a form is created with
                                a value of the id of the product from the database
                             -->
                             <input type="hidden" name="item" value="<?php echo  $row["sid"]?>">
                            <input type="hidden" name="amount" value="<?php echo  $row["amount"]?>">
                            <?php

                            //checks whether the product has been dispatched

                            if($row["paid"] == "notpaid"){

                                /**
                                 * if it has yet to be dispatched the button is activated
                                 * the form with the hidden input 
                                 * is redirected to "todispatch.php"
                                 */
                                
                            ?>
                                <button class="btn btn-primary btn-sm" type="submit" method="post">Pay</button>
                            <?php
                            }else{

                                /**
                                 * the button is deactivated/disabled if the product has already been dispatched
                                 */

                            ?>
                            <button class="btn btn-primary btn-sm" type="submit" disabled>Paid</button>
                            <?php
                            }
                            ?>
                        </form>
                    </td>

                    <td>
                        <form action="toarrival.php">
                            <input type="hidden" name="arrival" value="<?php echo  $row["sid"]?>">
                            <?php
                            //checks current status of the product
                            if($row["checkpoint"] == 5){
                                /**
                                 * If the product is at the final checkpoint designated
                                 * as 5
                                 * the button for arrival is enabled
                                 * the hidden form with the id of the specific product
                                 * is redirected to "to arrival.php"
                                 */
                            ?>
                                <button class="btn btn-primary btn-sm" type="submit" method="post">Arrival</button>
                            <?php
                            }else{
                                /**
                                 * else the button displayed is disabled
                                 * since the product is yet to arrive to it's
                                 * destination or has already arrived at it's
                                 * destination and marked as so
                                 * checkpoint 6
                                 */
                            ?>
                               <button class="btn btn-primary btn-sm" type="submit" disabled>Arrival</button>
                            <?php
                            }
                            ?>
                        </form>
                    </td>
                    <td>
                    <?php
                    echo '<img src="data:image/jpeg;base64,'.base64_encode( $row['image'] ).'" height = "40px" width ="40px"/>';
                    ?>
                    </td>
                </tr>
                <?php
                }
                ?>
            </tbody>
        </table>
        <?php
            } else {
                //in case there are no products within the database
                //this message is displayed instead of the table
            echo "<h3> There are no items currently in transit currently</h3>";
            }
            //the connection to the database is then closed for security purposes
            $conn->close();
        ?>
        
    </div>
</body>

</html>