<?php 
/**
 * Author - Gitau Dennis
 * Project - Courier Services
 * Section - Workflow
 * Description - This code enables the updating of
 *               records in the status page.
 
 */

 //Checks connection
	$conn = mysqli_connect("localhost:3306","root","","courier-services");
	
	if (mysqli_connect_errno())
		{
			echo "Failed to connect to MySQL: " . mysqli_connect_error();
		}
		else{
			echo "Worked";
		}
		session_start();
 //creates a session to help store the item id
	  	$itemid = $_SESSION['arrivalid'];
  		echo  $itemid;

		
  		$currcheck = $_SESSION['currcheck'];
  		$currcheck++;
  		echo $currcheck;

//Updates records on the status page.
	  $sql =  "UPDATE shippingdetails SET checkpoint ='$currcheck' WHERE sid = '$itemid'";
	  if ($conn->query($sql) === TRUE) {
		    echo "Record updated successfully";
			$conn->close();
			header("Location: status.php");
		} else {
		    echo "Error updating record: " . $conn->error;
		    $conn->close();
		}

 ?>