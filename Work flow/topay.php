<?php

session_start();

//acquiring the value of the item id from the hidden form
$itemid = $_GET["item"];
$amount = $_GET["amount"];

//created a session value "arrivalid"
//with the value of the item id
$_SESSION['item'] = $itemid;
$_SESSION['amount'] = $amount;

//helps check if the value of the product has been stored
//in the session
echo $_SESSION['item'];
echo $_SESSION['amount'];

//redirects to arrival.php with the "arrivalid" value of the product in the session

header("Location: ../Payment/Felix/account.php");
?>