
<?php
ob_start();
session_start();
$con = mysqli_connect("localhost:3306","root","","courier-services");
/*
**Author- Ghitao Mwangi
 * Project - Courier Services
 * Section - Workflow 
*/

// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }
  else{
      echo "Worked";
  }
  //creates a session to help store the item id
  $itemid = $_SESSION['arrivalid'];
  echo  $itemid;

  
  $sql = " SELECT * from shippingdetails s 
  inner join parcel p on s.parcelid = p.id
  inner join offices o on s.officeid = o.id
  inner join offices of on s.destinationid = of.id
  inner join users u on s.senderid = u.id
  inner join users us on s.recepientid = us.id
  inner join vehicle v on s.vehicleid = v.id where sid = '$itemid'";
  $result = $con->query($sql);

  mysqli_query($con, $sql);
  /*if ($result){
  echo "no";
  }else{
  echo "yes";
  }*/
 ?>

<!DOCTYPE html>
<!--
**starting html
-->
<html>

<head>
    <title>Speedy Courier Arrival</title>
    <link rel="stylesheet" type="text/css" href="Arrival.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim. min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>

<body>
    <div class="heading">
        <h1> ARRIVAL DESTINATION</h1>
    </div>
    <?php
      if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
        /*echo  $row["username"];
        echo $row["platenumber"];*/
        $_SESSION['currcheck'] = $row["checkpoint"];
    ?>

    <div class="container">
        <div class="row">
            <div class="col-sm-7">
                <section id="nav-bar">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="#"><img src="courier.jpg" height="500px" width="500px"></a>
                </nav>
                </section>
            </div>

            <div class="col-sm-5">
                <form action="arriving.php">
                <!--
                **where information will be displayed
                    -->
                    <div class="form-group">
                        <label for="Item">Item Name</label>
                        <input type="text" class="form-control"placeholder="<?php echo $row["name"] ?>"  name="item_name" disabled>
                    </div>
                    
                    <div class="form-group">
                        <label for="Item">Sender Name</label>
                        <input type="text" class="form-control"placeholder="<?php echo $row["username"]?>" name="sender_name" disabled>
                    </div>

                    <div class="form-group">
                        <label for="Item">Sender Cell No.</label>
                        <input type="text" class="form-control"placeholder="<?php echo $row["senderno"]?>" name="sender_cell_no" disabled>
                    </div>

                    <div class="form-group">
                        <label for="Item">Recepient Name</label>
                        <input type="text" class="form-control"placeholder="<?php echo $row["lastname"]?>" name="recepient_name" disabled>
                    </div>

                    <div class="form-group">
                        <label for="Item">Recepient Cell No.</label>
                        <input type="text" class="form-control"placeholder="<?php echo $row["receiverno"]?>" name="recepient_cell_no" disabled>
                    </div>

                    <button type="submit" name="submit" class="btn btn-primary">Update arrival status</button>
                </form>
        <?php
            }
          }

        ?>
            </div>
        </div>
    </div>
     <!--
         **sending data to the database
             -->

            <script type="text/javascript">
                   document.getElementsByTagName("button")[0].addEventListener("click", function (e) {
                       alert("Product arrived at destination"); 
});

            </script>

</body>

</html>