<?php
/**
 * Author - Steve Nginyo 
 * Project - Courier Services
 * Section - Workflow
 * Description - Helps place the item id of the product in a sesssion 
 *               in order to provide the checkpoint page with the specific id of
 *               the product that is to be updated to the database
 *               that it has arrived at a checkpoint
 * 
 */

//creates a session to help store the item id
session_start();

//acquiring the value of the item id from the hidden form
$itemid = $_GET["checkpoint"];

//created a session value "checkpointid"
//with the value of the item id
$_SESSION['checkpointid'] = $itemid;

//helps check if the value of the product has been stored
//in the session
echo $_SESSION['checkpointid'];

//redirects to Checkpoints.html with the "checkpointid" value of the product in the session
header("Location: Checkpoints.php");
?>