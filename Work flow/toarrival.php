<?php
/**
 * Author - Steve Nginyo 
 * Project - Courier Services
 * Section - Workflow
 * Description - Helps place the item id of the product in a session 
 *               in order to provide the arrival page with the specific id of
 *               the product that is to be updated to the database
 *               that it has arrived at it's destination
 * 
 */

//creates a session to help store the item id
session_start();

//acquiring the value of the item id from the hidden form
$itemid = $_GET["arrival"];

//created a session value "arrivalid"
//with the value of the item id
$_SESSION['arrivalid'] = $itemid;

//helps check if the value of the product has been stored
//in the session
echo $_SESSION['arrivalid'];

//redirects to arrival.php with the "arrivalid" value of the product in the session
header("Location: Arrival.php");
?>