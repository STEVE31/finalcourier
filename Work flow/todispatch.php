<?php
/**
 * Author - Steve Nginyo 
 * Project - Courier Services
 * Section - Workflow
 * Description - Helps place the item id of the product in a sesssion 
 *               in order to provide the dispatch page with the specific id of
 *               the product that is to be updated to the database
 *               that it has to be dispatched
 * 
 */

//creates a session to help store the item id
session_start();

//acquiring the value of the item id from the hidden form
$itemid = $_GET["dispatch"];

//created a session value "dispatchid"
//with the value of the item id
$_SESSION['dispatchid'] = $itemid;

//helps check if the value of the product has been stored
//in the session
echo $_SESSION['dispatchid'];

//redirects to dispatch.php with the "dispatchid" value of the product in the session
header("Location: dispatch.php");
?>